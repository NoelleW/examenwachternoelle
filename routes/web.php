<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use App\Post;

Route::get('dashboard', 'DashboardController@index');
Route::get('blog', 'DashboardController@blog')->name('blog');


// Create post
Route::get('submit', "DashboardController@store")->name('submit');

// Edit post
Route::get('edit/{id}', function ($id = null) {
    if ($id){
        Post::where('id', '=', $id)->first();

    }
    $post = App\Post::find($id);

    return view('edit')->with([

        'id' => $post->id,
        'title' => $post->title,
        'body' => $post->body

    ]);
});

//Send edited post
Route::get('edit/{id}/submit', 'DashboardController@update');


// remove post
Route::get('remove/{id}', function ($id = null) {
    if ($id){
        Post::where('id', '=', $id)->delete();
    }
    return redirect('dashboard');
});



Route::get('/home', function () {
    return redirect('dashboard');
});
// login auth etc
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Auth::routes();


Route::get('/', function () {
    return redirect('/blog');
});
