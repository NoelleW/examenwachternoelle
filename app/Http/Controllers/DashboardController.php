<?php

namespace App\Http\Controllers;


use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Post;

class DashboardController extends Controller
{

    public function index() {
        $posts = Post::orderByRaw('updated_at DESC')->get();
        return view('dashboard')->with('posts',$posts);

    }

    public function blog() {
        $posts = Post::orderByRaw('updated_at DESC')->get();
        return view('welcome')->with('posts',$posts);

    }
    //create
    public function store(Request $request)
    {
        // Validate the request...

        $post = new Post();
        $post->titel = $request->titel;
        $post->content = $request->body;
        $post->save();

        throw new HttpResponseException(redirect('dashboard')->with($post->titel, $post->content));
    }

    public function update(Request $request, $id)
    {
        // Validate the request....
        $post = Post::find($id);
        $post->titel = $request->titel;
        $post->content = $request->body;
        $post->save();

        throw new HttpResponseException(redirect('dashboard')->with($post->id, $post->titel, $post->content));
    }
}
