<html>
<head>
    <title>Laravel examen</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
@section('navigation')
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Laravel Blog</a>
            </div>
            <ul class="nav navbar-nav">

                <li><a href="{{ route('blog') }}">Blog</a></li>
                <li><a href="{{ route('login') }}">Login</a></li>
                <li><a href="{{ route('register') }}">Register</a></li>
            </ul>
        </div>
    </nav>
@show
<div class="container">
    @yield('content')

</div>
</body>
</html>