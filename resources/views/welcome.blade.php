@extends('layouts.master')

@section('content')

    <h3>Welcome op de blog </h3>
    @foreach($posts as $post)
        <table class="table">
            <tbody>
            <tr>
                <td> Titel: {{$post->titel}}</td>
            </tr>
            <tr>
                <td>Content: {{$post->content}}</td>
            </tr>
            <tr>
                <td>Laatste geüpdate: {{$post->updated_at}}</td>
            </tr>
            <tr>
                <td><a href="{{ url('edit/' . $post->id) }}">bewerk</a></td>
                <td><a href="{{ url('remove/' . $post->id) }}">verwijder</a></td>
            </tr>
            </tbody>
        </table>
    @endforeach
    @endsection