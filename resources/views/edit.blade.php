@extends('layouts.master')

@section('content')
<form action="{{ url('edit/' . $id. '/submit') }}" method="get">

    <div class="form-group">
        <label for="titel">Post Titel</label>
        <input type="text" class="form-control" id="titel" name="titel" required>
    </div>

    <div class="form-group">
        <label for="content">Post Content</label>
        <textarea class="form-control" id="body" name="body" required rows="3"></textarea>
    </div>

    <button type="submit" class="btn btn-primary">Publish bericht</button>

</form>
@endsection