@extends('layouts.master')

@section('content')
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{ asset('/sass/bootstrap.min.css') }}" rel="stylesheet">

        <title>Laravel</title>

    </head>
    <body>
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <h3>Creeër een nieuwe blog post</h3>
                        <form action="{{route("submit")}}" method="get">

                            <div class="form-group">
                                <label for="titel">Post Titel</label>
                                <input type="text" class="form-control" id="titel" name="titel" required>
                            </div>

                            <div class="form-group">
                                <label for="content">Post Content</label>
                                <textarea class="form-control" id="body" name="body" required rows="3"></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary">Publish bericht</button>

                        </form>
                    </div>

                    <div class="col-md-3">
                        <h3>Published blog posts: </h3>
                        @foreach($posts as $post)
                            <table class="table">
                                <tbody>
                                        <tr>
                                            <td> Titel: {{$post->titel}}</td>
                                        </tr>
                                        <tr>
                                            <td>Content: {{$post->content}}</td>
                                        </tr>
                                        <tr>
                                            <td>Laatste geüpdate: {{$post->updated_at}}</td>
                                        </tr>
                                        <tr>
                                            <td><a href="{{ url('edit/' . $post->id) }}">bewerk</a></td>
                                            <td><a href="{{ url('remove/' . $post->id) }}">verwijder</a></td>
                                        </tr>
                                </tbody>
                            </table>
                            @endforeach
                        </div>
                    </div>
            </div>

    @endsection
    </body>
</html>
